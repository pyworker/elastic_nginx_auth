# Настройка аутентификации elastic

## Установка и конфигурирование Nginx

Подготавливается конфигурационный файл `elastic.conf`, который помещается в `/etc/nginx/site-avalible`. На конфигураиционный файл делается символическая ссылка (`ln -s`) в `/etc/nginx/site-enabled`. Все конфигурационные файлы, распологающиеся в `/etc/nginx/site-enabled` nginx импортирует в главный конфигурационный файл `nginx.conf` (секция `include`) после чего запускается nginx. 

Конфигурационный файл elastic.conf
```
# /etc/nginx/site-avalible/elastic.conf

events {
    worker_connections  1024;
}

http {
    # new group of servers named "elasticsearch" 
    upstream elasticsearch {
        server 127.0.0.1:9200;
        # server 127.0.0.1:9201;
        # server 127.0.0.1:9202;
    }

    # rules for role: "all" (and non auth users)
    # allow HEAD /
    server {
        listen 8080;
        server_name localhost;

        # logging all requests
        access_log /var/log/nginx/elastic.access.log combined;
        error_log /var/log/nginx/elastic.error.log warn;

        # search location by template
        location / {
            # break connection, return error code 
            return 401;
        }

        # search location by equal
        location = / {
            if ($request_method !~ "HEAD") {
                return 403;
                break;
            }
            proxy_pass http://elasticsearch;
            proxy_redirect off;
        }
    }

    # rules for role: "users"
    # allow access to /_search and /_analyze 
    # and index "tmp_index" (add data, create mapping) 
    server {
        listen 8081;
        server_name localhost;

        # auth section
        auth_basic           "Elasticsearch Users";
        auth_basic_user_file users;

        location / {
            return 403;
        }

        location ~* ^(/_search|/_analyze|/tmp_index) {
            proxy_pass http://elasticsearch;
            proxy_redirect off;
        }
    }

    # rules for role: "admins"
    # allow access to anything
    server {
        listen 8082;
        server_name localhost;

        auth_basic           "Elasticsearch Admins";
        auth_basic_user_file admins;

        location / {
            proxy_pass http://elasticsearch;
            proxy_redirect off;
        }
    }
}
```

Доступ к страндартному порту elastic (`9200`) рекомендуется ограничить с помощью `iptable` или `firewall`.   
Перенаправление всего поступающего трафика к elastic на нестандартный порт позволяет:

- скрыть наличие elastic
- обесаечить журналирование всех поступающих запрос

После изменений в конфигурационном файле рекомендуется перезагрузить nginx
```bash
service ngin restart
```


**Запуск nginx выполняется следующей командой**
```
$ nginx -p $PWD/nginx/ -c $PWD/nginx_http_auth_deny_path.conf
# flags:
# -p  set prefix path (default: /usr/share/nginx/)
# -с  set configuration file (default: /etc/nginx/nginx.conf)
```

**Запуск nginx в docker контейнере**
```
$ docker run --name mynginx -v /host/path/nginx.conf:/etc/nginx/nginx.conf:ro -v /host/path/log:/var/log/nginx/ -d -t -p 8080:80 nginx
```


## Регистрация новых пользователей 
Информация о всех авторизированных пользователях хранится в файле в виде: `пользователь: пароль`. 

1. Создадим файл `/etc/nginx/users` для хранения информации о пользовапателях.
2. Добавим нового пользователя `ubu`
```bash
$ sudo sh -c "echo -n 'ubu:' >> /etc/nginx/users"
enter password: 12345
confirm password: 12345
```
или:
```bash
printf "ubu:$(openssl passwd -crypt 12345)n" >> /etc/nginx/users
```
3. Добавим новую шифрованную запись пароля в для пользователя 
```bash
$ sudo sh -c "openssl passwd -apr1 >> /etc/nginx/users"
```
4. Проверим, что информация о пользователе записалась корректно
```bash
$ cat /etc/nginx/users
ubu:$apr1$wI1/T0nB$jEKuTJHkTOOWkopnXqC1d1
```


## Разраобка и запуск клиента с аутентификацией

Авторизация пользователей может осуществляться с использованием стандартной библиотеки `elasticsearch-py` для это пишется следующий код

```python
from elasticsearch import Elasticsearch, TransportError
from elastic_conf import HOSTS

class ElasticCluster:
    def __init__(self, index, type):
        self._index = index
        self._type = type
        try:
            self._es = Elasticsearch(hosts=HOSTS, http_auth=('sammy', '12345'))
        except TransportError as transport_err:
            # if HOSTS not for elastic
            raise ConnectionError(
                    'cant connect to elastic hosts:"%s" error:%s'
                    % (self._hosts, transport_err.error))

    def add(self, document):
        pass

    def update(self, document):
        pass

    def delete(self, global_id):
        pass


if __name__ == '__main__':
    el = ElasticCluster(index='tmp_index', type='auth')
        .   .   .

```


В результате эксперимента:

1. При попытке писать в порт 8080 выскакивает ошибка аутентификации.
2. При попытке писать или искать данные в индексе отличном от `tmp_index` вылетает ошибка авторизации.
3. Запись и поиск документов пр обращении к портам `8081` и `8082` осущесьвляется корректно.


## Применительно к проекту
В проекте выделяется три роли: пользователь, администратор ТРС и администратор СКЦ. Для этих ролей задаются следующие права доступа: 

1. Доступ к индексу user и system имеют только административные роли
2. Доступ к индексу task имеют все роли
