import json
from elastic_cluster import ElasticCluster


def create_document():
    """
    prepare document for elastic
    """
    return json.dumps({
        'name': 'bob',
        'sername': 'momo'
    })


def search_document():
    """
    create search query to elastic
    """
    return json.dumps({
        'version': True,
        'query': {
            'term': {'name': 'bob'}}})


def main():
    """
    main logic: test auth in elastic
    """
    el = ElasticCluster(index='test_auth', type='auth')
    document = create_document()
    query = search_document()
    try:
        el.add(document=document)
        print(el.find(search_query=query))
    except Exception as e:
        raise e


if __name__ == '__main__':
    main()