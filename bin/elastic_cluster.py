from elasticsearch import Elasticsearch, TransportError
from elastic_conf import HOSTS


class ElasticCluster:
    def __init__(self, index, type):
        self._index = index
        self._type = type
        try:
            self._es = Elasticsearch(hosts=HOSTS, http_auth=('sammy', '12345'),)
        except TransportError as transport_err:
            # if HOSTS not for elastic
            raise ConnectionError('cant connect to elastic hosts: "%s" error: "%s"' % (self._hosts, transport_err.error))

    def add(self, document):
        """
        add new document to elastic index
        Args:
            document: dict, new document
        Returns: 
            True: if task added to the elastic index
            False: if task NOT added to the elastic index
        """

        #
        # doesnt work with auth
        #
        # checking for cluster availability 
        # if not self._es.ping():
        #     raise ConnectionError(
        #         'failed to add document: elastic hosts %s not available'
        #         % HOSTS)

        try:
            # add new document to index
            server_response = self._es.index(
                index=self._index,
                doc_type=self._type,
                body=document)

        except TransportError as transport_err:
            raise ValueError('failed to add:%s' % (transport_err))
        return server_response.get('_id') if server_response.get('_shards').get('successful') else None

    def add_with_id(self, document, gid):
        """
        add new document to elastic index 
            with gid value as elastic_id
        Args:
            document: dict, new document
            gid: str, global ID for new document
        Raises:
            Exception: transport error
        Returns:
        """
        if not self._es.ping():
            raise ConnectionError(
                'failed to add document: elastic hosts %s not available'
                % HOSTS)

        try:
            server_response = self._es.index(
                index=self._index,
                doc_type=self._type,
                body=document,
                op_type='create',
                id=gid)

        except TransportError as transport_err:
            raise Exception('failed to add document: %s' % transport_err)
        return server_response.get('_id') if server_response.get('_shards').get('successful') else None


    def update(self, version, global_id, query):
        """
        update elastic document
        Args:
            version: int, document version
            global_id: str, document elastic ID
            query: dict, elastic query
        Returns:
            True: if document updated
            False: if document not changed
        """
        if not self._es.ping():
            raise ConnectionError(
                'failed to update document: elastic hosts "%s" not available'
                % HOSTS)

        try:
            server_response = self._es.update(
                index=self._index,
                doc_type=self._type,
                id=global_id,
                body=query,
                fields=["_source", "_id", "_version"],
                refresh=True,
                version=version)

            result = True if server_response.get('_shards').get('successful') else False
        
        except RequestError as request_err:
            raise Exception(request_err.info)

    def find(self, search_query):
        """
        find documents by search query
        Args:
            search_query: query for search in elastic
        Raises: 
        Returns:
            List of Hits object: if find results 
            []: if no find results
        """

        # checking for cluster availability and running search_query
        # if not self._es.ping():
        #     raise ConnectionError(
        #         'failed to find document: hosts "%s" not available'
        #         % HOSTS)

        try:
            server_response = self._es.search(
                index=self._index,
                doc_type=self._type,
                body=search_query
            )
        
        except TransportError as elastic_transport:
            return []
        except RequestError as request_err:
            return []

        return server_response
        # server_response = ElasticResponse(server_response)
        # return server_response.hits if server_response.total else []


    def del_doc(self, gid):
        """
        delete document from elastic
        Args:
            gid: str, global document ID
        Raises:
        Returns:
            True: if document deleted from elastic index
            False: if document wasnt delete from elastic index
        """

        if not self._es.ping():
            raise ConnectionError('failed to delete document: hosts "%s" not available' % HOSTS)

        try:
            server_response = self.es.delete(
                index=self._index,
                doc_type=self._type,
                id=gid,
                refresh=True)

            return True

        except TransportError as transport:
            if (
                transport.info.get('result') == 'not_found' and 
                transport.info.get('_shards').get('successful') == 1):
                return False
            else:
                raise Exception('failed to delete document: %s' % transport.error)

    def create_index(self, mapping):
        """
        create mapping for index
        Args:
            mapping: dict, mapping scheme for index
        Raises:
        Returns:
        """

        if not self._es.ping():
            raise ConnectionError('failed to add center %s: elastic hosts %s not available' % (center_id, HOSTS))

        try:
            self._es.indices.create(index=self._index, body=mapping)

        except TransportError as transport_err:
            raise Exception('failed to create mappig for new index:%s' % transport_err)


    def delete_index(self):
        """
        delete index from elastic
        Args:
            True: if index deleted
            False: if index NOT deleted
        """
        return self._es.indices.delete(index=self._index)